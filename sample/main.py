import osmnx as ox
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time
import matplotlib.image as mpimg
from map_generator import  mapGenerator, entityGeneration, setVehicle, setBuildings
import threading
from loading_msg import loading_message, loading_graph, killMessage
from goldorak import Goldorak
import offer
from proposal import Proposal
import random

timeT = 0
text_aff1 = None
text_aff2 = None
txt = ""
num_vehicles_arrived = []
error = "Not implemented yet"
pending_proposals = [] # tableau des Propositions en traitememnt
proposals_not_accepted = [] # tableau des Propositions non acceptées ou échues
proposals_accepted = [] # tableau des Propositions acceptées

def error():
    killMessage('error')
    raise ValueError(error)

sim_mod = input("Veuillez saisir le mode d'exécution pour la simulation :\n1 pour l'utilisation d'une image préfabriquée.\n2 pour l'utilisation d'une image récupérée par satellite\nVotre choix : ")

match sim_mod:
    case 'test':
        loading_thread = threading.Thread(target=loading_graph, args=("1",)) 
        loading_thread.start()   
        bbox = [45.809673224, 45.715801576, 4.900783118, 4.769284582]
        graph = ox.graph_from_bbox(bbox=bbox, network_type='drive')
        goldo = Goldorak(graph)
        folder = "img/premade/" 
        folder += 'Lyon'
        path = folder + ".png"
        img = mpimg.imread(path)
        fig, ax = ox.plot_graph(graph, show=False, close=False, node_color='none', node_size=0, edge_linewidth=0.25, edge_color='white')

        killMessage('end')
        all_entity = entityGeneration(goldo, graph, {"Car" : 2, "Van" : 1, "Truck" : 1}, 2)
        cars = all_entity["Cars"]
        buildings = all_entity["Building"]

    case '1':
        name_city = input("Veuillez saisir le nom de la ville pour débuter la simulation\nListe des villes disponibles :\n  Lyon\n  Paris\nVotre choix : ")
        loading_thread = threading.Thread(target=loading_graph, args=("1",)) 
        loading_thread.start()        
        if name_city not in ['Lyon', 'Paris']:
            killMessage('error')
            raise ValueError("Choix invalide. La ville choisit n'est pas dans la liste. Si vous avez entrer un ville présente dans la liste n'oublier pas la majuscule.")
        match name_city:
            case 'Lyon':
                bbox = [45.809673224, 45.715801576, 4.900783118, 4.769284582]
            case 'Paris':
                bbox = [48.903640593999995, 48.814138506, 2.420117798, 2.2223723019999997]
        graph = ox.graph_from_bbox(bbox=bbox, network_type='drive')
        goldo = Goldorak(graph)
        folder = "img/premade/" 
        folder += name_city
        path = folder + ".png"
        img = mpimg.imread(path)
        fig, ax = ox.plot_graph(graph, show=False, close=False, node_color='none', node_size=0, edge_linewidth=0.25, edge_color='white')

        killMessage('end')
        vbox = setVehicle()
        nbB = setBuildings()
        all_entity = entityGeneration(goldo, graph, vbox, nbB)
        cars = all_entity["Cars"]
        buildings = all_entity["Building"]

    case '2':
        name_city = input("Veuillez saisir le nom de la ville pour débuter la simulation\nVeuillez mettre une majuscule à la première lettre\nVotre choix : ")
        loading_thread = threading.Thread(target=loading_graph, args=("2",))
        loading_thread.start()
        bbox = mapGenerator(name_city)
        graph = ox.graph_from_bbox(bbox=bbox, network_type='drive')
        goldo = Goldorak(graph)
        #graph = ox.graph_from_bbox(bbox[0], bbox[1], bbox[2], bbox[3], network_type='drive')
        folder = "img/generate/" 
        folder += name_city
        path = folder + ".png"
        img = mpimg.imread(path)
        fig, ax = ox.plot_graph(graph, show=False, close=False, node_color='none', node_size=0, edge_linewidth=0.25, edge_color='white')

        killMessage('end')
        vbox = setVehicle()
        nbB = setBuildings()
        all_entity = entityGeneration(goldo, graph, vbox, nbB)
        cars = all_entity["Cars"]
        buildings = all_entity["Building"]

    case _:
        raise ValueError("Choix invalide. Veuillez saisir soit 1 pour une image préfabriquée, soit 2 pour une image récupérée par satellite.")


# Fonction pour mettre à jour tous les éléments de l'animation
def update(frame):
    global num_vehicles_arrived, timeT, text_aff1, text_aff2, txt

    timeT += 1
    goldo.set_current_time(timeT)
    texte1 = f'Tick actuel : {timeT}'  
    texte2 = f'Vehicle arrived by ID in Order : {txt}'
    if text_aff1 is not None:
        text_aff1.remove()
    text_aff1 = plt.text(4.8861, 45.80175, texte1, color='white', ha='right', va='top')

    for car in cars:
        car.update()   # Vérifier si le véhicule a atteint sa destination
        if len(car.path) == 0 and car.position == car.target:
            #execution de la prochaine offre
            if len(car.offers_list) > 0:
                next_offer = car.offers_list.pop(0).get_arrival_point()
                car.move_to(next_offer)

    #generation des nouvelles propositions
    for building in buildings:
        pending_proposals.append(Proposal(building.subit_new_offer(), timeT + 20, building))

    #update des propositions en cours
    for proposal in pending_proposals:
        #phase de negociation coté vehicules
        random.shuffle(cars)
        for car in cars:
            #recherche de la discussion du vehicule dans la proposition
            discuss_index = proposal.discuss_index_from_vehicle(car)
            #creation de la discussion du vehicule si elle n'existe pas
            if discuss_index < 0:
                proposal.add_discussion(car)
                discuss_index = len(proposal.discussions) - 1
            #selection de la discussion
            discuss = proposal.get_disc(discuss_index)
            if discuss.get_responses_left() >= 0 and not discuss.get_vehicle_accepted():
                can_afford = car.can_afford(discuss.get_last_offer())
                if can_afford :
                    #ajout du trajet pour aller chercher l'offre
                    car.add_offer(offer.Offer(car.get_last_offer_arrival_point(), discuss.get_last_offer().get_departure_point(), 0, 0, -1, goldo))
                    #ajout de l'offre
                    car.add_offer(discuss.get_last_offer())
                    #acceptation de l'offre
                    discuss.set_vehicle_accepted(True)
                    # deplace les propositions acceptées
                    proposals_accepted.append(proposal)
                    pending_proposals.remove(proposal)
                    break
                elif discuss.get_responses_left > 0:
                    #generation d'une contre-offre
                    discuss.add_offer(car.generate_offer(discuss))
                    #Passage de relais
                    discuss.set_vehicle_accepted(True)
                    discuss.set_building_accepted(False)
        #phase de negociation coté batiment
        #selection du batiment proposant
        current_building = proposal.get_proposing_building()
        for discuss in proposal.discussions:
            if ( discuss.get_responses_left() >= 0 and not discuss.get_building_accepted() ):
                    if current_building.can_afford(discuss.get_last_offer()):
                        # ajout du trajet pour aller chercher l'offre
                        discuss.get_vehicle_that_discuss().add_offer(offer.Offer(car.get_last_offer_arrival_point(), discuss.get_last_offer().get_departure_point(), 0, 0, -1))
                        # ajout de l'offre
                        discuss.get_vehicle_that_discuss().add_offer(discuss.get_last_offer())
                        # acceptation de l'offre
                        discuss.set_building_accepted(True)
                        # deplace les propositions acceptées
                        proposals_accepted.append(proposal)
                        pending_proposals.remove(proposal)
                        break
                    else:
                        if discuss.get_responses_left() > 0:
                            # generation d'une contre-offre
                            discuss.add_offer(current_building.generate_offer(discuss))
                            # Passage de relais
                            discuss.set_building_accepted(True)
                            discuss.set_vehicle_accepted(False)
        proposals_lists_clean_up()
        print()
        print("Time : ", timeT)
        for car_index in range(len(cars)):
            for i in range(len(cars[car_index].get_offers_list())):
                print("         car", car_index, "offer ", i, " offers : ", cars[car_index].get_offers_list()[i])





    
    if text_aff2 is not None:
        text_aff2.remove()
    text_aff2 = plt.text(4.8860, 45.79900, texte2, color='white', ha='right', va='top')

    # Vérifier si tous les véhicules ont terminé leurs trajets
    if len(num_vehicles_arrived) == len(cars):
        ani.event_source.stop()  # Arrêter l'animation
        plt.close()
        temps_fin = time.time()
        temps_execution = temps_fin - temps_debut  
        print('')
        print(timeT)
        print(temps_execution)
    return [car.marker for car in cars]

def proposals_lists_clean_up(): #nettoyge d'apres iteration
    for proposal in pending_proposals:
        #deplace toutes les propositions qui ont un nombre de message supérieur à 5 et celles dont la deadline est dépassée dans le tableau des propositions non acceptées
        if timeT > proposal.get_acceptation_deadline() or not proposal.is_still_discussing():
            proposals_not_accepted.append(proposal)
            pending_proposals.remove(proposal)

x_min, x_max = plt.xlim()
y_min, y_max = plt.ylim()

plt.imshow(img, extent=[x_min, x_max, y_min, y_max], alpha=1)

# Créer et affiche l'animation
ani = FuncAnimation(fig, update, frames=30 ,blit=True, interval=0)
#exec_thread = threading.Thread(target=loading_message, args=("En cours d'exécution",))
#exec_thread.daemon = True
#exec_thread.start()

temps_debut = time.time()
plt.show()

if __name__ == '__main__':
    goldo.setup()
    goldo.run()

