import sys
import time

global stop
global load

def loading_message(message):
    global load
    global stop
    load = True
    stop = 0
    time.sleep(0.2)
    chars = "/—\|"
    i = 0
    while load:
        sys.stdout.write('\r' + message + ' ' + chars[i % len(chars)])
        sys.stdout.flush()
        time.sleep(0.1)
        i += 1
    print('')

def loading_graph(case):
    global stop 
    stop = 1
    match case:
        case '1':
            message = 'Graph en cours de génération'
            end = "Graph chargé"
        case '2':
            message = 'Carte et Graph en cours de génération'
            end = "Carte et Graph chargé"
    chars = "/—\|"
    i = 0
    while stop == 1:
        sys.stdout.write('\r' + message + ' ' + chars[i % len(chars)])
        sys.stdout.flush()
        time.sleep(0.1)
        i += 1
    if stop == 0:
        print('')
        print(end)
        print('')
    elif stop == 2:
        print('')

def killMessage(type):
    global stop, load
    match type:
        case 'error':
            stop = 2
            load = False
        case 'end':
            stop = 0
            load = False
            print("\n")