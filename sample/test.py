import osmnx as ox
import matplotlib.pyplot as plt


city = "New York, USA"
graph = ox.graph_from_place(city, network_type='drive')

fig, ax = ox.plot_graph(graph, show=False, close=False)

x_min, x_max = plt.xlim()
y_min, y_max = plt.ylim()

print ("x_min : " + str(x_min))
print ("x_max : " + str(x_max))
print ("y_min : " + str(y_min))
print ("y_min : " + str(y_max))


plt.show()