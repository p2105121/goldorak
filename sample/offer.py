import vehicle
import osmnx as ox


class Offer: #devrait en realité s'appeler "course"
    def __init__(self, departure_point, arrival_point, price, parcel_amount, achievement_deadline, goldorak_reference):
        self.departure_point = departure_point # noeud/coos de depart
        self.arrival_point = arrival_point # noeud/coos d'arrivée
        self.price = price # prix (float/double)
        self.parcel_amount = parcel_amount # nombre de colis transportés
        self.achievement_deadline = achievement_deadline # date limite d'acceptation
        self.goldorak_instance = goldorak_reference

    def get_price(self):
        return self.price

    def get_parcel_amount(self):
        return self.parcel_amount

    def get_departure_point(self):
        return self.departure_point

    def get_arrival_point(self):
        return self.arrival_point

    def get_achievement_deadline(self):
        return self.achievement_deadline


    def distance(self):
        vhcl = vehicle.Vehicle(self.goldorak_instance, self.goldorak_instance.graph, 9999, self.departure_point, 'car')
        #distance between the departure and the arrival points
        return vhcl.get_dist(ox.nearest_nodes(vhcl.graph, self.departure_point[1], self.departure_point[0]), ox.nearest_nodes(self.goldorak_instance.graph, self.arrival_point[1], self.arrival_point[0]))

    def duration(self, speed):# speed is the speed of the vehicle
        return self.distance() / speed

    def negotiate(self, entity):
        if entity.can_afford(self.price):
            entity.accept_offer(self)
        else:
            entity.reject_offer(self)

    def __str__(self):
        return f"(Off from{self.departure_point} to {self.arrival_point} with {self.parcel_amount} par @{self.price}€ ratio: {self.price/self.distance()} dead {self.achievement_deadline})"
    def __repr__(self):
        return self.__str__()