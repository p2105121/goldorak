import ee
import requests
from PIL import Image
from io import BytesIO
import os
from geopy.geocoders import Nominatim
import random
from vehicle import Vehicle
from building import Building
import time

def get_city_coordinates(city_name):
    geolocator = Nominatim(user_agent="my_app")
    location = geolocator.geocode(city_name)
    if location:
        latitude = location.latitude
        longitude = location.longitude
        return latitude, longitude
    else:
        return None

def get_square_coordinates(city_name, radius=0.1):
    city_coords = get_city_coordinates(city_name)
    if city_coords:
        lat, lon = city_coords
        min_lat = lat - radius
        max_lat = lat + radius
        min_lon = lon - radius
        max_lon = lon + radius
        square_coords = [max_lat, min_lat, max_lon, min_lon]
        return square_coords
    else:
        return None

def mapGenerator(name):
    os.environ["GOOGLE_CLOUD_PROJECT"] = "eegoldo"
    square_coords = get_square_coordinates(name)
    if square_coords:
        # Initialisation de l'API Earth Engine
        ee.Initialize(project='eegoldo')

        # Coordonnées de la zone d'intérêt
        min_longitude = square_coords[3] 
        min_latitude = square_coords[1]  
        max_longitude = square_coords[2]  
        max_latitude = square_coords[0]  

        # Définition d'une région d'intérêt spécifique
        roi = ee.Geometry.Rectangle([min_longitude, min_latitude, max_longitude, max_latitude])

        # Chargement de l'image Landsat
        image = ee.ImageCollection('LANDSAT/LC08/C01/T1_SR') \
            .filterBounds(roi) \
            .filterDate('2021-01-01', '2021-12-31') \
            .sort('CLOUD_COVER') \
            .first() \
            .clip(roi)

        # Rééchantillonner l'image à une projection spécifique
        image = image.reproject('EPSG:4326', None, 30)

        # Affichage de l'image
        url = image.getThumbUrl({'bands': ['B4', 'B3', 'B2'], 'min': 0, 'max': 3000})

        # Téléchargement de l'image
        response = requests.get(url)

        # Ouverture de l'image avec PIL
        img = Image.open(BytesIO(response.content))

        # Sauvegarde de l'image en tant que fichier PNG
        folder = 'img/generate/'
        folder += name
        name = folder + '.png'
        img.save(name)
        return square_coords

    else:
        print("Ville introuvable ou erreur lors de la récupération des coordonnées.")



def entityGeneration(goldo, graph, vbox, nbB):
    initial_poss= []
    id_tot = 0
    buildings = []
    cars = []

    for i in range(nbB):
        random_node = random.choice(list(graph.nodes()))  
        initial_poss.append((graph.nodes[random_node]['y'], graph.nodes[random_node]['x']))
        id_tot += 1
        buildings.append(Building(graph, i, initial_poss[i],goldo))
    
    initial_poss= []

    for i in range(int((sum(vbox.values())))):
        random_node = random.choice(list(graph.nodes()))  # Choix d'un nœud aléatoire
        initial_poss.append((graph.nodes[random_node]['y'], graph.nodes[random_node]['x']))
        if (vbox["Car"] > 0):
            cars.append(Vehicle(goldo, graph, id_tot+i, initial_poss[i], 'car'))
            vbox["Car"] -= 1
        elif (vbox["Van"] > 0):
            cars.append(Vehicle(goldo, graph, id_tot+i, initial_poss[i], 'van'))
            vbox["Van"] -= 1
        elif (vbox["Truck"] > 0):
            cars.append(Vehicle(goldo, graph, id_tot+i, initial_poss[i], 'truck'))
            vbox["Truck"] -= 1

    for building in buildings:
        building.place()

    for car in cars:
        random_node = random.choice(list(graph.nodes()))
        random_position = graph.nodes[random_node]['y'], graph.nodes[random_node]['x']
        car.move_to(random_position)
    return {"Cars" : cars, "Building" : buildings}


def setVehicle():
    time.sleep(1)
    nbCar = input("Veuillez saisir le nombre de voitures pour la simulation\nVotre choix : ")
    nbVan = input("Veuillez saisir le nombre de camionnettes pour la simulation\nVotre choix : ")
    nbTruck = input("Veuillez saisir le nombre de camions pour la simulation\nVotre choix : ")
    return {"Car" : int(nbCar), "Van" : int(nbVan), "Truck" : int(nbTruck)}

def setBuildings():
    return int(input("Veuillez saisir le nombre de batiments pour la simulation\nVotre choix : "))