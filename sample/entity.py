
class Entity:
    def __init__(self, graph, id, position, color, goldorak_reference):
        self.graph = graph
        self.id = id
        self.position = position
        self.color = color
        self.goldorak_instance = goldorak_reference

    def can_afford(self, offer):#retourne un booléen evaluant si l'entitée peut "supporter" le prix
        pass