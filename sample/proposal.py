from discussion import Discussion
class Proposal:
    def __init__(self, start_offer, deadline, proposing_building):
        self.offer_p = start_offer
        self.proposing_building = proposing_building
        self.acceptation_deadline_p = deadline # date limite d'acceptation
        self.discussions = [] # tableau des discussions (une discussion par vehicule

    def add_discussion(self, discuss):
        self.discussions.append(discuss)

    def get_disc(self,index):
        return self.discussions[index]

    def get_acceptation_deadline(self):
        return self.acceptation_deadline_p

    def get_proposing_building(self):
        return self.proposing_building

    def set_proposing_building(self, building):
        self.proposing_building = building

    def is_still_discussing(self):

        for discuss in self.discussions:
            if discuss.get_responses_left() > 0:
                return True
        return False

    def discuss_index_from_vehicle(self, vehicle):
        for i in range(len(self.discussions)):
            if self.discussions[i].get_vehicle_that_discuss() == vehicle:
                return i
        return -1
    def add_discussion(self, car):
        self.discussions.append(Discussion(self.offer_p))
        self.discussions[-1].set_vehicle_that_discuss(car)

    def __str__(self):
        return "__Proposal__: " + '\n' + "start offer: " + str(self.offer_p) + '\n' +"deadline: " + str(self.acceptation_deadline_p) + '\n' + "proposing building: " + str(self.proposing_building) + '\n' + "discussions: " + str(self.discussions) + '\n'