from entity import Entity
from offer import Offer
from goldorak import Goldorak
import osmnx as ox
import matplotlib.pyplot as plt  
import networkx as nx
import numpy as np
from geopy.distance import geodesic



class Vehicle(Entity):
    def __init__(self, goldorak_reference, graph, id, initial_position, type):
        if type == 'car':
            color = 'red'
            self.speed = 20 #in meter/tick ~= 72km.h-1
            self.capacity = 4
        elif type == 'van':
            color = 'blue'
            self.speed = 15 #in meter/tick ~= 54km.h-1
            self.capacity = 8
        elif type == 'truck':
            color = 'green'
            self.speed = 10 #in meter/tick ~= 36km.h-1
            self.capacity = 12
        else:
            print("Error: invalid vehicle type")
        super().__init__(graph, id, initial_position, color, goldorak_reference)

        self.node = ox.nearest_nodes(graph, initial_position[1], initial_position[0])
        self.path = []
        self.pathNTN = []
        self.path_nodeToNode = []
        self.current_node_path = self.node
        self.distBtwnPoint = 20
        self.target = self.node
        self.marker, = plt.plot([], [], marker='o', color=self.color, markersize=10)

        self.price_distance_ratio = 2 #the minimum ratio the vehicle can handle
        self.goldorak_instance = goldorak_reference
        self.offers_list = [] #liste des offres qui restent a traiter ( certaines offres sont factices et representent des trajets entre les offres reelles. Les offres factices ont un prix nul)
        self.vehicle_id = goldorak_reference.vehicle_id_couter

    def get_dist(self, initial_node, target_node):
        dist_acc = 0
        start_coord = (self.graph.nodes[initial_node]['y'], self.graph.nodes[initial_node]['x'])
        end_coord = (self.graph.nodes[target_node]['y'], self.graph.nodes[target_node]['x'])
        interpolated_points = list(zip(np.linspace(start_coord[0], end_coord[0], num=self.distBtwnPoint),
                                        np.linspace(start_coord[1], end_coord[1], num=self.distBtwnPoint)))
        while (len(interpolated_points) > 1):
            distance = geodesic(interpolated_points[0], interpolated_points[1]).meters
            dist_acc += distance
            interpolated_points.pop(0)
        return dist_acc 
    
    def set_pathNTN(self, target_position):
        target_node = ox.nearest_nodes(self.graph, target_position[1], target_position[0])
        self.pathNTN= nx.shortest_path(self.graph, self.node, target_node, weight='length')
        self.target = self.graph.nodes[target_node]['y'], self.graph.nodes[target_node]['x']
    
    def set_pathToNode(self, initial_node, target_node):
        start_coord = (self.graph.nodes[initial_node]['y'], self.graph.nodes[initial_node]['x'])
        end_coord = (self.graph.nodes[target_node]['y'], self.graph.nodes[target_node]['x'])
        dist = self.get_dist(initial_node, target_node)
        meter_point = int(dist)
        interpolated_points = list(zip(np.linspace(start_coord[0], end_coord[0], num=meter_point),
                                        np.linspace(start_coord[1], end_coord[1], num=meter_point)))
        self.path_nodeToNode.extend(interpolated_points)
        if (len(self.path_nodeToNode) > 2):
            self.path_nodeToNode.pop(0)
            
    def move_to(self, target_position):
        self.set_pathNTN(target_position)
        self.set_pathToNode(self.pathNTN[0], self.pathNTN[1])
        while (len(self.pathNTN) > 1):
            self.set_pathToNode(self.pathNTN[0], self.pathNTN[1])
            self.pathNTN.pop(0)
            while (len(self.path_nodeToNode) !=0 ):
                self.path.append(self.path_nodeToNode[0])
                self.path_nodeToNode.pop(0)
        self.filtre_liste(self.speed)

    def filtre_liste(self, nb_a_enlever):
        liste_resultat = []
        taille_liste = len(self.path)
        i = 0
        while i < taille_liste:
            if i + nb_a_enlever < taille_liste:
                liste_resultat.append(self.path[i])
                i += nb_a_enlever + 1
            else:
                liste_resultat.append(self.path[i])
                if i + 1 < taille_liste:
                    liste_resultat.append(self.path[-1])
                break
        self.path = liste_resultat

    def update(self):
        if len(self.path) > 0:
            current_position = self.path[0]
            self.position = current_position
            self.marker.set_data([current_position[1]], [current_position[0]])
            self.path.pop(0)

    def can_afford(self, offer_o):#retourne un booléen evaluant si le vehicule peut "supporter" le prix
        #new_offer = Offer(self.offers_list[-1], offer_o.arrival_point, 0, 0, offer_o.achievement_deadline ) #trajet pour aller chercher l'offre
        #l'offre est acceptable si le prix est inferieur au budget du vehicule et si le trajet est faisable avant l'echéance
        return offer_o.price >= self.price_distance_ratio * offer_o.distance() #and offer_o.achievement_deadline - self.goldorak_instance.current_time  <= offer_o.duration(self.speed)) + new_offer.duration(self.speed)

    def generate_offer(self, discuss): #genere une offre en augmentant un peu le prix de la derniere offre recue
        o = discuss.negociation[-1]
        return Offer(o.get_departure_point() , o.get_arrival_point(), o.get_price() * 0.9, o.get_parcel_amount(), o.get_achievement_deadline() + 1 )

    def get_offers_list(self):
        return self.offers_list

    def get_last_offer_arrival_point(self):
        if len(self.offers_list) == 0:
            return self.position
        return self.offers_list[-1].get_arrival_point()

    def add_offer(self, offer):
        self.offers_list.append(offer)
