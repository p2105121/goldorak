from offer import Offer


class Discussion:
    def __init__(self, start_offer):
        self.max_discussion_time = 5
        self.offer = start_offer
        self.negociation = []  # tableau d'offres (controffres et réponses)
        self.acceptation_deadline_d = 0  # date limite d'acceptation
        self.building_accepted = True #etat instantané de l'acceptation par le batiment. Cela permet de savoir a qui est le tour de parler (si les deux sont acceptées, la discussion est terminée)
        self.Vehicle_accepted = False #etat instantané de l'acceptation par le vehicule. Cela permet de savoir a qui est le tour de parler (si les deux sont acceptées, la discussion est terminée)
        self.vehicle_that_discuss = None #reference au vehicule qui discute dans cette instance de discussion

    def add_offer(self, offer):
        self.negociation.append(offer)

    def get_offer(self):
        return self.offer

    def get_negociation_offer(self, index):
        return self.negociation[index]

    def get_last_offer(self):
        if len(self.negociation) == 0:
            return self.offer
        return self.get_negociation_offer(-1)

    def get_acceptation_deadline(self):
        return self.acceptation_deadline_d

    def get_responses_left(self):
        return self.max_discussion_time - len(self.negociation)

    def get_building_accepted(self):
        return self.building_accepted

    def set_building_accepted(self, bool):
        self.building_accepted = bool

    def get_vehicle_accepted(self):
        return self.Vehicle_accepted

    def set_vehicle_accepted(self, bool):
        self.Vehicle_accepted = bool


    def get_vehicle_that_discuss(self):
        return self.vehicle_that_discuss

    def set_vehicle_that_discuss(self, vehicle):
        self.vehicle_that_discuss = vehicle

    def is_already_accepted(self):
        return self.building_accepted and self.Vehicle_accepted

    def get_vehicle_that_discuss(self):
        return self.vehicle_that_discuss

    def __str__(self):
        return f"(Disc\nstart offer:{str(self.offer) }\nnegociation: {str(self.negociation)} \ndead: {str(self.acceptation_deadline_d)} \nbuilding Accepted?:  {str(self.building_accepted)} Vehicle Accepted?:  {str(self.Vehicle_accepted)} \ndiscussing vehicle:{str(self.vehicle_that_discuss)}\n"
    def __repr__(self):
        return self.__str__()