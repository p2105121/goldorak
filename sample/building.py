from entity import Entity
import matplotlib.pyplot as plt
import random
import osmnx as ox
from offer import Offer
import vehicle

class Building(Entity):
    def __init__(self, graph, id, position, goldorak_reference):
        super().__init__(graph, id, position, 'black', goldorak_reference)
        self.marker, = plt.plot([], [], marker='s', color=self.color, markersize=15)

        self.parcel_amount = 0
        self.price_distance_ratio = 5 #the maximum ratio the Building can handle


    def get_parcel_amount(self):
        return self.parcel_amount

    def set_parcel_amount(self, parcel_amount):
        self.parcel_amount = parcel_amount

    def place(self):
        self.marker.set_data([self.position[1]], [self.position[0]])
        
    def create_offer(self, departure_point, price, duration): #créée une offre
        pass

    def can_afford(self, offer): #retourne un booléen evaluant si le batiment peut "supporter" le prix
        return offer.price <= self.price_distance_ratio * offer.distance()

    def generate_offer(self, discuss):
        #genere une offre en augmentant un peu le prix de la derniere offre recue
        o = discuss.negociation[-1]
        return Offer(o.get_departure_point() , o.get_arrival_point(), o.get_price() * 1.1, o.get_parcel_amount(), o.get_achievement_deadline() + 1 )
    def subit_new_offer(self):
        vhcl = vehicle.Vehicle(self.goldorak_instance, self.graph, 9999, self.position, 'car')#useless vehicle generation to get the distance
        random_node = random.choice(list(self.graph.nodes()))  # Choix d'un nœud aléatoire
        arrival_pos = ((self.graph.nodes[random_node]['y'], self.graph.nodes[random_node]['x']))
        offer = Offer(self.position,
                      arrival_pos,
                      vhcl.get_dist(ox.nearest_nodes(self.graph, self.position[1], self.position[0]), ox.nearest_nodes(self.graph, arrival_pos[1], arrival_pos[0])) * self.price_distance_ratio * 0.55,
                      min(random.randint(1, 12),self.parcel_amount),
                      self.goldorak_instance.current_time + 20,
                      self.goldorak_instance)
        return offer

    def __str__(self):
        return f"(Bui@{self.position} with {self.parcel_amount} par, ratio:{self.price_distance_ratio})"