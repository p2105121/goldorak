from building import Building
#from Offer import Offer
from proposal import Proposal
import numpy as np

from entity import Entity
import random

class Goldorak:

    def __init__(self,graph):
        #self
        self.graph = graph
        self.current_time = 0
        self.vehicle_id_couter = 0
        self.vehicles = [] # tableau des vehicules
        self.buildings = [] # tableau des batiments
        self.pending_proposals = [] # tableau des Propositions en traitememnt
        self.proposals_not_accepted = [] # tableau des Propositions non acceptées ou échues
        self.proposals_accepted = [] # tableau des Propositions acceptées

    #please write functions to add vehicles, buildings and proposals
    def add_vehicle(self, vehicle):
        self.vehicles.append(vehicle)
    def add_building(self, building):
        self.buildings.append(building)
    def add_proposal(self, proposal):
        self.pending_proposals.append(proposal)

    def set_current_time(self, time):
        self.current_time = time


    def clean_up(self): #nettoyge d'apres iteration
        #deplace toutes les propositions qui ont un nombre de message supérieur à 5 et celles dont la deadline est dépassée dans le tableau des propositions non acceptées
        for proposal in self.pending_proposals:
            for discuss in proposal.discussions:
                if discuss.get_vehicle_accepted() and discuss.get_building_accepted():
                    self.proposals_accepted.append(proposal)
                    self.pending_proposals.remove(proposal)
                    break
            if self.current_time > proposal.get_acceptation_deadline() or proposal.is_still_discussing() == False:
                self.proposals_not_accepted.append(proposal)
                self.pending_proposals.remove(proposal)


    def setup(self):
        #generation des batiments
        #generation des vehicules
        pass
    def run(self):
        pass