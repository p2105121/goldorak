PYTHON = python3
PIP = pip3
CACHE_DIR = cache/ sample/cache sample/__pycache__
SRC_DIR = sample
MAIN_FILE = main.py

ROOT_DIR = .
DOXYFILE = Doxyfile
DOXYGEN_OUTPUT = $(ROOT_DIR)/docs
DOXYGEN = doxygen

.PHONY: all clean

all: $(CACHE_DIR) install-dependencies clean
	$(PYTHON) $(SRC_DIR)/$(MAIN_FILE)

install-dependencies:
	$(PIP) install -r requirements.txt

$(CACHE_DIR):
	mkdir -p $(CACHE_DIR)

docs:
	$(DOXYGEN) $(DOXYFILE)

clean:
	rm -rf $(CACHE_DIR) $(DOXYGEN_OUTPUT)






